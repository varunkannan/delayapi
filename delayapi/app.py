#!/usr/bin/env python3
"""
Copyright (c) 2019 Riptide IO, Inc.
All Rights Reserved.
"""
from sys import argv

from riptide.main import main


__all__ = ["main"]


if __name__ == "__main__":
    main(argv=argv)


# __END__
