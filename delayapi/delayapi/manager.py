"""
Copyright (c) 2019 Riptide IO, Inc.
All Rights Reserved.
"""
import logging
import time

from riptide.properties import get_properties

LOGGER = logging.getLogger(__name__)


class delayapiManager(object):

    def __init__(self):
        super(delayapiManager, self).__init__()
        self._running = False


    @property
    def name(self):
        return self.__class__.__name__

    def start(self):
        self._running = True

    def stop(self):
        self._running = False

    def list(self, query_params=None, **kwargs):
        sleep_time = int(get_properties().get("RESPONSE_DELAY") or 0)
        LOGGER.warning("Sleeping for {}".format(sleep_time))
        time.sleep(sleep_time)
        return "success"

    def get(self, group_id, query_params=None, **kwargs):
        LOGGER.info("{} {}; group_id={}, query_params={}, kwargs={}".format(
            self.name, self.list.__name__, group_id, query_params, kwargs
        ))
        return "delayapi"

    def create(self, **config):
        LOGGER.info("{} {}; config={}".format(
            self.name, self.list.__name__, config
        ))
        return ["delayapi"]

    def update(self, group_id, **config):
        LOGGER.info("{} {}; group_id={} config={}".format(
            self.name, self.list.__name__, group_id, config
        ))
        return ["delayapi"]

    def remove(self, group_id, query_params=None):
        LOGGER.info("{} {}; group_id={} query_params={}".format(
            self.name, self.list.__name__, group_id, query_params
        ))


# __END__
