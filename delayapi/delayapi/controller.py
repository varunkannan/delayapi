"""
Copyright (c) 2019 Riptide IO, Inc.
All Rights Reserved.
"""
import logging

from riptide.web.contract import AuthenticationAware
from riptide.web.util.controller import CRUDController


LOGGER = logging.getLogger(__name__)


class delayapiController(CRUDController, AuthenticationAware):
    def authenticate(self, http_method, action, *args, **kwargs):
        return True

# __END__
