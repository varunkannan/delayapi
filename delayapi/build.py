"""
Copyright (c) 2019 Riptide IO, Inc.
All Rights Reserved.
"""

from pybuilder.core import use_plugin, init

use_plugin("python.core")
use_plugin("python.install_dependencies")
use_plugin("python.flake8")
use_plugin("python.distutils")
use_plugin("python.pycharm")
use_plugin("copy_resources")
use_plugin("pypi:pybuilder_pytest")


name = "delayapi"
version = "0.0.1"
author = "Riptide Developer"
summary = "delayapi"
description = "delayapi"

default_task = ["analyze", "publish"]


@init
def set_properties(project):

    project.build_depends_on('pytest-cov')

    project.depends_on_requirements("requirements.txt")

    project.build_depends_on_requirements("requirements-dev.txt")

    project.set_property("install_dependencies_extra_index_url",
                         "http://pip.riptideio.com/simple/")

    project.set_property("install_dependencies_trusted_host",
                         "pip.riptideio.com")

    project.set_property("dir_source_main_python", "delayapi")

    project.set_property("dir_source_pytest_python", "tests")

    project.set_property("run_unit_tests_propagate_stdout", True)

    project.set_property("run_unit_tests_propagate_stderr", True)

    project.get_property("pytest_extra_args").append("-x")

    project.get_property("pytest_extra_args").append(
        "--cov-report=term-missing"
    )

    project.get_property("pytest_extra_args").append("--cov=delayapi")

    project.set_property("coverage_threshold_warn", 0)

    project.set_property("flake8_break_build", True)

    project.set_property("flake8_include_scripts", True)

    project.set_property("flake8_include_test_sources", True)

    project.set_property("flake8_max_line_length", 79)

    project.set_property("flake8_verbose_output", True)

    project.set_property("distutils_upload_repository", "internal")

    project.include_file("delayapi", "data/context.xml")

    project.include_file("delayapi", "data/project.properties")

    project.include_file("delayapi", "data/routes.json")

    project.include_file("delayapi", "data/validators.xml")


# __END__


# __END__