import mock

from riptide.web.util.controller import CRUDController
from delayapi.controller import delayapiController


def test_delayapiController():
    req = mock.MagicMock()
    req.body = {}
    controller_obj = delayapiController(
        mock.MagicMock(),
        req,
        test="something"
    )
    assert isinstance(controller_obj, CRUDController)
    print("--------------> test_delayapiController Done!")


# __END__
